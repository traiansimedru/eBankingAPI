package traian.ebanking.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import traian.ebanking.model.account.Account;
import traian.ebanking.model.account.AccountChangeStateBody;
import traian.ebanking.service.account.AccountService;

@RestController
@AllArgsConstructor
@RequestMapping("api/account")
public class AccountController {
    private final AccountService accountService;

    @GetMapping("get/{accountNumber}")
    public Account getByAccountNumber(@PathVariable Integer accountNumber) {
        return accountService.get(accountNumber);
    }

    @PostMapping("add")
    public void add(@RequestBody Account newAccount) {
        accountService.add(newAccount);
    }

    @PatchMapping("change")
    public void changeState(@RequestBody AccountChangeStateBody accountChangeStateBody) {
        accountService.changeState(accountChangeStateBody);
    }
}
