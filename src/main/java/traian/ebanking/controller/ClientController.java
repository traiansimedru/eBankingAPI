package traian.ebanking.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import traian.ebanking.model.client.Client;
import traian.ebanking.service.client.ClientService;

@RestController
@AllArgsConstructor
@RequestMapping("api/client")
public class ClientController {
    private final ClientService clientService;

    @GetMapping("get/{userName}")
    public Client get(@PathVariable String userName) {
        return clientService.get(userName);
    }

    @PostMapping("signup")
    public void signup(@RequestBody Client newClient) {
        clientService.signup(newClient);
    }
}
