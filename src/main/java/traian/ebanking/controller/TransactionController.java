package traian.ebanking.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import traian.ebanking.model.transaction.Transaction;
import traian.ebanking.model.transaction.TransactionTopupBody;
import traian.ebanking.model.transaction.TransactionTransferBody;
import traian.ebanking.service.transaction.TransactionService;

@RestController
@AllArgsConstructor
@RequestMapping("api/transaction")
public class TransactionController {
    private final TransactionService transactionService;

    @GetMapping("get/all")
    public List<Transaction> getAll() {
        return transactionService.getAll();
    }

    @PostMapping("topup")
    public void topup(@RequestBody TransactionTopupBody transactionTopupBody) {
        transactionService.topup(transactionTopupBody);
    }

    @PostMapping("transfer")
    public void transfer(@RequestBody TransactionTransferBody transactionTransferBody) {
        transactionService.transfer(transactionTransferBody);
    }
}
