package traian.ebanking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import traian.ebanking.model.client.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    Client findByUserName(String userName);
}
