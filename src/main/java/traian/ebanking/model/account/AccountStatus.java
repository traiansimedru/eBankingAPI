package traian.ebanking.model.account;

public enum AccountStatus {
    ACTIVE, IN_PENDING, DISABLED
}
