package traian.ebanking.model.account;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static traian.ebanking.model.account.AccountStatus.ACTIVE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Account")
@Table(name = "accounts")
public class Account {
    @Id
    @SequenceGenerator(name = "account_sequence", sequenceName = "account_sequence", allocationSize = 1)
    @GeneratedValue(strategy = SEQUENCE, generator = "account_sequence")
    @JsonProperty("id")
    @Column(name = "id", columnDefinition = "serial")
    private Long id;

    @JsonProperty("account_number")
    @Column(name = "account_number", columnDefinition = "int", unique = true, nullable = false)
    private Integer accountNumber;

    @JsonProperty("bank")
    @Column(name = "bank", columnDefinition = "varchar(255)", nullable = false)
    private String bank;

    @JsonProperty("client_user_name")
    @Column(name = "client_user_name", columnDefinition = "varchar(255)", nullable = false)
    private String clientUserName;

    @JsonProperty("sum")
    @Column(name = "sum", columnDefinition = "numeric")
    private Double sum = 0d;

    @Enumerated(STRING)
    @JsonProperty("status")
    @Column(name = "status", columnDefinition = "varchar(255)")
    private AccountStatus status = ACTIVE;

    public Account(Integer accountNumber, String bank,
            String clientUserName, Double sum,
            AccountStatus status) {
        this.accountNumber = accountNumber;
        this.bank = bank;
        this.clientUserName = clientUserName;
        this.sum = sum;
        this.status = status;
    }
}
