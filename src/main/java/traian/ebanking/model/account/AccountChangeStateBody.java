package traian.ebanking.model.account;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountChangeStateBody {
    @JsonProperty("account_number")
    private Integer accountNumber;

    @JsonProperty("status")
    private AccountStatus status;
}
