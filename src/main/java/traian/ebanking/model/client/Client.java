package traian.ebanking.model.client;

import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Client")
@Table(name = "clients")
public class Client {
    @Id
    @SequenceGenerator(name = "client_sequence", sequenceName = "client_sequence", allocationSize = 1)
    @GeneratedValue(strategy = SEQUENCE, generator = "client_sequence")
    @JsonProperty("id")
    @Column(name = "id", columnDefinition = "serial")
    private Long id;

    @JsonProperty("first_name")
    @Column(name = "first_name", columnDefinition = "varchar(255)", nullable = false)
    private String firstName;

    @JsonProperty("last_name")
    @Column(name = "last_name", columnDefinition = "varchar(255)", nullable = false)
    private String lastName;

    @JsonProperty("user_name")
    @Column(name = "user_name", columnDefinition = "varchar(255)", unique = true, nullable = false)
    private String userName;

    @JsonProperty("password")
    @Column(name = "password", columnDefinition = "varchar(255)", nullable = false)
    private String password;

    public Client(String firstName, String lastName,
            String userName, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
    }
}
