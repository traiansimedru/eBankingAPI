package traian.ebanking.model.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionTransferBody {
    @JsonProperty("source_account_number")
    private Integer sourceAccountNumber;

    @JsonProperty("destination_account_number")
    private Integer destinationAccountNumber;

    @JsonProperty("transaction_sum")
    private Double transactionSum;
}
