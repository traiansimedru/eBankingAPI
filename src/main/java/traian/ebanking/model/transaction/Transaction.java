package traian.ebanking.model.transaction;

import static javax.persistence.GenerationType.SEQUENCE;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Transaction")
@Table(name = "transactions")
public class Transaction {
    @Id
    @SequenceGenerator(name = "transaction_sequence", sequenceName = "transaction_sequence", allocationSize = 1)
    @GeneratedValue(strategy = SEQUENCE, generator = "transaction_sequence")
    @JsonProperty("id")
    @Column(name = "id", columnDefinition = "serial")
    private Long id;

    @JsonProperty("operation_date")
    @Column(name = "operation_date", columnDefinition = "date")
    private Date operationDate = Date.valueOf(LocalDate.now());

    @JsonProperty("source_account_number")
    @Column(name = "source_account_number", columnDefinition = "int", nullable = false)
    private Integer sourceAccountNumber;

    @JsonProperty("source_account_final_balance")
    @Column(name = "source_account_final_balance", columnDefinition = "numeric", nullable = false)
    private Double sourceAccountFinalBalance;

    @JsonProperty("destination_account_number")
    @Column(name = "destination_account_number", columnDefinition = "int")
    private Integer destinationAccountNumber;

    @JsonProperty("destination_account_final_balance")
    @Column(name = "destination_account_final_balance", columnDefinition = "numeric", nullable = false)
    private Double destinationAccountFinalBalance;

    @JsonProperty("transaction_sum")
    @Column(name = "transaction_sum", columnDefinition = "numeric", nullable = false)
    private Double transactionSum;

    public Transaction(Integer sourceAccountNumber, Double sourceAccountFinalBalance,
            Integer destinationAccountNumber, Double destinationAccountFinalBalance,
            Double transactionSum) {
        this.sourceAccountNumber = sourceAccountNumber;
        this.sourceAccountFinalBalance = sourceAccountFinalBalance;
        this.destinationAccountNumber = destinationAccountNumber;
        this.destinationAccountFinalBalance = destinationAccountFinalBalance;
        this.transactionSum = transactionSum;
    }

    public Transaction(Date operationDate, Integer sourceAccountNumber,
            Double sourceAccountFinalBalance, Integer destinationAccountNumber,
            Double destinationAccountFinalBalance, Double transactionSum) {
        this.operationDate = operationDate;
        this.sourceAccountNumber = sourceAccountNumber;
        this.sourceAccountFinalBalance = sourceAccountFinalBalance;
        this.destinationAccountNumber = destinationAccountNumber;
        this.destinationAccountFinalBalance = destinationAccountFinalBalance;
        this.transactionSum = transactionSum;
    }
}
