package traian.ebanking.model.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionTopupBody {
    @JsonProperty("source_account_number")
    private Integer sourceAccountNumber;

    @JsonProperty("transaction_sum")
    private Double transactionSum;
}
