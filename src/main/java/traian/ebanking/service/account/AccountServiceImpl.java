package traian.ebanking.service.account;

import static java.lang.String.format;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import traian.ebanking.model.account.Account;
import traian.ebanking.model.account.AccountChangeStateBody;
import traian.ebanking.repository.AccountRepository;
import traian.ebanking.service.client.ClientService;

@Service
@Transactional
@AllArgsConstructor
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final ClientService clientService;

    @Override
    public Account get(Integer accountNumber) {
        Account account = accountRepository.findByAccountNumber(accountNumber);

        if (account == null) {
            throw new IllegalStateException(String.format("Account with number %s is not found", accountNumber));
        }

        return account;
    }

    @Override
    public void add(Account newAccount) {
        Account account = accountRepository.findByAccountNumber(newAccount.getAccountNumber());
        clientService.get(newAccount.getClientUserName());

        if (account != null) {
            throw new IllegalStateException(format("Account with number %s exist", newAccount.getAccountNumber()));
        }

        accountRepository.save(newAccount);
    }

    @Override
    public void changeState(AccountChangeStateBody accountChangeStateBody) {
        Account account = accountRepository.findByAccountNumber(accountChangeStateBody.getAccountNumber());
        account.setStatus(accountChangeStateBody.getStatus());
    }
}
