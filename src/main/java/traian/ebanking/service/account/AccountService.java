package traian.ebanking.service.account;

import traian.ebanking.model.account.Account;
import traian.ebanking.model.account.AccountChangeStateBody;

public interface AccountService {
    Account get(Integer accountNumber);

    void add(Account newAccount);

    void changeState(AccountChangeStateBody accountChangeStateBody);
}
