package traian.ebanking.service.transaction;

import static java.lang.String.format;

import java.util.List;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import traian.ebanking.model.account.Account;
import traian.ebanking.model.transaction.Transaction;
import traian.ebanking.model.transaction.TransactionTopupBody;
import traian.ebanking.model.transaction.TransactionTransferBody;
import traian.ebanking.repository.AccountRepository;
import traian.ebanking.repository.TransactionRepository;
import traian.ebanking.service.account.AccountService;

@Service
@AllArgsConstructor
public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepository transactionRepository;
    private final AccountService accountService;
    private final AccountRepository accountRepository;

    @Override
    public List<Transaction> getAll() {
        return transactionRepository.findAll();
    }

    @Override
    public void topup(TransactionTopupBody transactionTopupBody) {
        Account account = accountService.get(transactionTopupBody.getSourceAccountNumber());

        if (transactionTopupBody.getTransactionSum() < 0) {
            throw new IllegalStateException(format("Sum must be greater than 0"));
        }

        account.setSum(account.getSum() + transactionTopupBody.getTransactionSum());

        transactionRepository.save(new Transaction(transactionTopupBody.getSourceAccountNumber(), account.getSum(),
                transactionTopupBody.getSourceAccountNumber(), account.getSum(),
                transactionTopupBody.getTransactionSum()));
    }

    @Override
    public void transfer(TransactionTransferBody transactionTransferBody) {
        Account sourceAccount = accountService.get(transactionTransferBody.getSourceAccountNumber());
        Account destinationAccount = accountRepository
                .findByAccountNumber(transactionTransferBody.getDestinationAccountNumber());

        if (destinationAccount == null) {
            sourceAccount.setSum(sourceAccount.getSum() + 0);
        } else if (sourceAccount.getSum() < transactionTransferBody.getTransactionSum()) {
            throw new IllegalStateException(format("Insufficient funds"));
        } else {
            sourceAccount.setSum(sourceAccount.getSum() - transactionTransferBody.getTransactionSum());
            destinationAccount.setSum(destinationAccount.getSum() + transactionTransferBody.getTransactionSum());

            transactionRepository
                    .save(new Transaction(transactionTransferBody.getSourceAccountNumber(), sourceAccount.getSum(),
                            transactionTransferBody.getDestinationAccountNumber(), destinationAccount.getSum(),
                            transactionTransferBody.getTransactionSum()));
        }
    }
}
