package traian.ebanking.service.transaction;

import java.util.List;

import traian.ebanking.model.transaction.Transaction;
import traian.ebanking.model.transaction.TransactionTopupBody;
import traian.ebanking.model.transaction.TransactionTransferBody;

public interface TransactionService {
    List<Transaction> getAll();

    void topup(TransactionTopupBody transactionTopupBody);

    void transfer(TransactionTransferBody transactionTransferBody);
}
