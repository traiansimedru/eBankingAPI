package traian.ebanking.service.client;

import traian.ebanking.model.client.Client;

public interface ClientService {
    Client get(String userName);

    void signup(Client newClient);
}
