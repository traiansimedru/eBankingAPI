package traian.ebanking.service.client;

import static java.lang.String.format;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import traian.ebanking.model.client.Client;
import traian.ebanking.repository.ClientRepository;

@Service
@Transactional
@AllArgsConstructor
public class ClientServiceImpl implements ClientService, UserDetailsService {
    private final ClientRepository clientRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Client get(String userName) {
        Client client = clientRepository.findByUserName(userName);

        if (client == null) {
            throw new IllegalStateException(format("Client with user name %s is not found", userName));
        }

        return client;
    }

    @Override
    public void signup(Client newClient) {
        Client client = clientRepository.findByUserName(newClient.getUserName());

        if (client != null) {
            throw new IllegalStateException(format("Client with user name %s exist", newClient.getUserName()));
        }

        newClient.setPassword(passwordEncoder.encode(newClient.getPassword()));
        clientRepository.save(newClient);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Client client = clientRepository.findByUserName(username);

        if (client == null) {
            throw new UsernameNotFoundException(format("Client with user name %s is not found", username));
        }

        return new User(client.getUserName(), client.getPassword(), new ArrayList<>());
    }
}
